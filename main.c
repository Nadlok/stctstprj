#include "pspGen.h"
#include "getSync.h"

int main()
{
    printf("********* Start stcTstPrj *********\n\n");

    /* Get PSP struct */
    sBinArray binArr;
    F(K_DEF, M_DEF, &binArr, true);

    /* Find sync marker */
    sList posList;
    getSync(&binArr, syncMarker, 1, &posList);

    /* Print result */
    printf("\nList - SyncMarker position\n\n");
    for(uint16_t i = 0; i < posList.numOfSM; i++) {

        printf("%03d. Position: %04d\tDistance: %04d\n",
               i,
               posList.data[i].position,
               posList.data[i].distance);
    }

    return 0;
}
