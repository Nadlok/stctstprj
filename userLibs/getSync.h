#ifndef GETSYNC_H
#define GETSYNC_H

/* Includes */
#include "pspGen.h"

/* Defines */
#define IS_CORRECT_HT(EXPR) (EXPR >=0 && EXPR <= SYNC_LEN)

/* Enums */

/* Structs */
typedef struct {

    uint16_t distance;
    uint16_t position;
} sPosList;

typedef struct {

    uint16_t numOfSM;   // Number of sync marker
    sPosList *data;     // Info about sync marker
} sList;

/* Function prototypes */
void getSync(const sBinArray *binArr, const uint8_t *syncM, const uint8_t hT, sList *res);

#endif // GETSYNC_H
