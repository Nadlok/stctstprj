#include "pspGen.h"

// Sync marker
uint8_t syncMarker[SYNC_LEN] = {0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1};

/** \brief Generate periodic binary sequence
 *
 * \param K: number of periods
 * \param M: shift syncmarker
 * \param res: return pointer to sBinArray struct
 * \param testFlag: if true than PSP data is 0
 * \return void
 *
 */
void F(const uint8_t K, const uint8_t M, sBinArray *res, bool testFlag) {

    srand(time(NULL));

#ifdef PRINT_DINFO
    printf("Call F(%d, %d, 0x%08x, %d)\n", K, M, res, testFlag);
#endif // PRINT_DINFO

    assert(res);
    assert(IS_CORRECT_K(K));
    assert(IS_CORRECT_M(M));

    uint8_t lenSyncMark = sizeof(syncMarker);

    res->binNum = M + (PSP_LEN + lenSyncMark) * K;
    res->binArr = (uint8_t *) malloc(res->binNum);

    memset(res->binArr, 0x00, res->binNum);

    /* Add start shift
    */
    for(uint8_t i = 0; i < M; i++) {

        *(res->binArr + i) = rand() % 2;
    }

    /* Add periodic sequence
    */
    for(uint8_t j = 0; j < K; j++) {

        /* Add sync marker
        */
        memcpy(&res->binArr[M + j * (lenSyncMark + PSP_LEN)], syncMarker, lenSyncMark);

        /* Add random bits
        */
        uint8_t genData = 0;
        for(uint8_t i = 0; i < PSP_LEN; i++) {

            genData = 0;
            if(!testFlag) {

                genData = rand() % 2;
            }

            *(res->binArr + i + (M + lenSyncMark) + j * (lenSyncMark + PSP_LEN)) = genData;
        }
    }
}
