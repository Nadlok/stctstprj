#ifndef PSPGEN_H
#define PSPGEN_H

/* Includes */
#include "common.h"

/* Defines */
#define K_MIN 1     // Minimum number of periods
#define K_MAX 100   // Maximum number of periods
#define K_DEF 25    // Default value
#define M_MIN 0     // Minimum value of shift
#define M_MAX 64    // Maximum value of shift
#define M_DEF 17    // Default value

#define PSP_LEN 64  // Length of periodic psp

#define SYNC_LEN 16

#define IS_CORRECT_K(EXPR) (EXPR >= K_MIN && EXPR <= K_MAX)
#define IS_CORRECT_M(EXPR) (EXPR >= M_MIN && EXPR <= M_MAX)

/* Extern */
extern uint8_t syncMarker[SYNC_LEN];

/* Enums */

/* Structs */
typedef struct {

    uint16_t binNum;
    uint8_t *binArr;
} sBinArray;

/* Function prototypes */
void F(const uint8_t K, const uint8_t M, sBinArray *res, bool testFlag);

#endif // PSPGEN_H
