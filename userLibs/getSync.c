#include "getSync.h"

/** \brief
 *
 * \param binArr sBinArray*: pointer to psp
 * \param syncM uint8_t*: pointer to sync marker array
 * \param hT uint8_t: threshold hamming distance
 * \param res sPosList*: return pointer to sPosList
 * \return void
 *
 */
void getSync(const sBinArray *binArr, const uint8_t *syncM, const uint8_t hT, sList *res) {

#ifdef PRINT_DINFO
    printf("Call getSync(0x%08x, 0x%08x, %d, 0x%08x)\n", binArr, syncM, hT, res);
#endif // PRINT_DINFO

    assert(binArr);
    assert(syncM);
    assert(res);
    assert(IS_CORRECT_HT(hT));

    uint16_t curPos = 0;
    uint16_t hd = 0;
    uint16_t cmpVal = 0;

    /* Allocation memory
    */
    res->numOfSM = 0;
    res->data = (sPosList *) malloc(sizeof(sPosList));

    /* Clear memory
    */
    res->data->distance = 0;
    res->data->position = 0;

    uint8_t *shiftArr = (uint8_t *) malloc(SYNC_LEN);

    /* Get syncMarker value
    */
    uint16_t refVal = 0;
    for(uint8_t i = 0; i < SYNC_LEN; i++) {

        refVal |= *(syncM + SYNC_LEN - 1 - i) << i;
    }

    /* Compare refVal with input data
    */
    while(curPos < (binArr->binNum - SYNC_LEN)) {

        memcpy(&shiftArr[0], &binArr->binArr[curPos], SYNC_LEN);

        /* Get value to compare
        */
        cmpVal = 0;
        for(uint8_t i = 0; i < SYNC_LEN; i++) {

            cmpVal |= *(shiftArr + SYNC_LEN - 1 - i) << i;
        }

        /* Compute hamming distance
        */
        hd = 0;
        uint16_t diff = ~cmpVal & refVal;
        for(uint8_t i = 0; i < SYNC_LEN; i++) {

            hd += (diff >> i) & 0x01;
        }

        /* Compare hamming distance with threshold
        */
        if(hd <= hT) {

            ((sPosList *)(res->data) + res->numOfSM)->distance = hd;
            ((sPosList *)(res->data) + res->numOfSM)->position = curPos;

            res->numOfSM++;
            res->data = (sPosList *)realloc(res->data, (1 + res->numOfSM) * sizeof(sPosList));

            /* Clear memory
            */
            ((sPosList *)(res->data) + res->numOfSM)->distance = 0;
            ((sPosList *)(res->data) + res->numOfSM)->position = 0;
        }

        curPos++;
    }

    free(shiftArr);
}
