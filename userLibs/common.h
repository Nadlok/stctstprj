#ifndef COMMON_H
#define COMMON_H

/* Includes */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

/* Defines */
#define USE_ASSERT
#define PRINT_DINFO

/* Enums */

/* Structs */

/* Function prototypes */

/* Other */
#ifdef  USE_ASSERT
    #include <assert.h>
#else
    #define assert_param(expr) ((void)0U)
#endif /* USE_FULL_ASSERT */

#endif // COMMON
